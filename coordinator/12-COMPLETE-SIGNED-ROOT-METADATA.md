# Complete generation of root metadata

## Role: Coordinator

## Purpose: This ceremony completes the process of preparing signed Root metadata.

## Steps:

* **DO** Add the signatures:
    ```bash
    $ rugged add-root-signature /opt/rugged/root_pubkey.pem /opt/rugged/root_signature.bin --key-type=pem
    $ rugged add-root-signature /opt/rugged/root1_pubkey.pem /opt/rugged/root1_signature.bin --key-type=pem
    ````

* **DO** Check that the contents of the now-complete root metadata conform to expectations:
    ```bash
    $ rugged show-partial-root-metadata
    ```

* **DO** Ensure that output conforms to expectations:
    ```
    Retrieving partial root metadata for version 1 (1.root.json).
    === METADATA ===
    Expires in 364 days,
    === SIGNATURES ===
    Signatures: 2
    Threshold: 1
    Signature 1 of 2: signed by root 1/2 -- UNVERIFIED (keyid:
    Signature 2 of 2: signed by root 2/2 -- UNVERIFIED (keyid:
    === KEYS ===
    snapshot 1/1 (ed25519) keyid:
    root 2/2 (ed25519) keyid:
    timestamp 1/1 (ed25519) keyid:
    root 1/2 (ed25519) keyid:
    targets 1/1 (ed25519) keyid:
    === ROLES ===
    root: 2 of 1 keys (keyids:
    snapshot: 1 of 1 keys (keyid:
    targets: 1 of 1 keys (keyid:
    timestamp: 1 of 1 keys (keyid:
    ```

@TODO: Add steps:
  - commit the result to ceremony branch
  - merge the ceremony branch
