# Initialize TUF repository with signed Root metadata

This ceremony describes the initial creation of a Rugged TUF metadata
repository by generating and signing Root metadata, and using that as the basis
to sign metadata for each of the subsequent Roles.

## Steps


In order to host TUF metadata using HSM-based root keys, we need to include those keys in root metadata and then add signatures from those HSMs.

This runbook contains multiple stages that will need to be accomplished by people serving in one of two roles:
* The **TUF Administrator** is responsible for coordinating the process, and performing certain steps that involve interacting with Rugged directly.
* The **Key-holders** are responsible for ensuring that the off-line root keys are kept secure, as well as using those keys to sign root metadata.

All participants should be familiar with [Runbook notation](../#notation).

## Prepare the environment

* **DO** [Install Rugged](TBD) on the Preparation computer
    * Check that Rugged is working as expected:
    ```shell
    rugged
    ```

* **DO** Create the directory in which Rugged will create partial root metadata:
    ```bash
    $ mkdir tuf_repo/partial
    $ sudo chown 440 tuf_repo/partial
    $ mkdir tuf_repo/tmp
    $ sudo chown 440 tuf_repo/tmp
    ```

* **DO** Ensure that the following files do not exist:
    * `tuf_repo/partial/1.root.json`
    * `tuf_repo/partial/signable-1.root.json`

    We will generate these files in the following steps. So we want to confirm that we have a clean environment.

* **DO** Ensure proper ownership of key directories:
    ```bash
    $ sudo chown 440 signing_keys/ -R
    $ sudo chown 440 verification_keys/ -R
    ```

* **DO** Ensure that access to the repository that stores the artifacts that resulted from the [key-generation ceremony](TBD)

## Generate online keys (TUF)

* **DO** Generate the `snapshot` keypair:
    ```shell
    rugged generate-keys --local --role=snapshot
    ```
* **DO** Generate the `targets` keypair:
    ```shell
    rugged generate-keys --local --role=targets
    ```
* **DO** Generate the snapshot keypair:
    ```shell
    rugged generate-keys --local --role=timestamp
    ```

## Generate partial root metadata

* **DO** Initialize new partial root metadata:

    ```shell
    rugged initialize-partial-root-metadata
    ```

* **DO** Add online verification keys:
    ```shell
    rugged add-verification-key snapshot /var/rugged/verification_keys/snapshot/snapshot.pub

    rugged add-verification-key targets /var/rugged/verification_keys/targets/targets.pub

    rugged add-verification-key timestamp /var/rugged/verification_keys/timestamp/timestamp.pub
    ```

* **DO** Add HSM-generated verification keys:
    ```shell
    rugged add-verification-key root /opt/rugged/root_pubkey.pem --key-type=pem
    rugged add-verification-key root /opt/rugged/root1_pubkey.pem --key-type=pem
    ```

* **DO** Check that the contents of the partial root metadata conform to expectations:
    ```shell
    rugged show-partial-root-metadata
    ```
    This should result in output like:
    ```text
    Retrieving partial root metadata for version 1 (1.root.json).
    === METADATA ===
    Expires in 364 days, 23 hours and 59 minutes
    === SIGNATURES ===
    Signatures: 0
    Threshold: 1
    === KEYS ===
    root 2/2 (ed25519) keyid:
    root 1/2 (ed25519) keyid:
    timestamp 1/1 (ed25519) keyid:
    targets 1/1 (ed25519) keyid:
    snapshot 1/1 (ed25519) keyid:
    === ROLES ===
    root: 2 of 1 keys (keyids:
    snapshot: 1 of 1 keys (keyid:
    targets: 1 of 1 keys (keyid:
    timestamp: 1 of 1 keys (keyid:
    ```

## Send partial root metadata for Key-holder signatures

* **DO** Ensure that the following files exist:
    * `tuf_repo/partial/1.root.json`
    * `tuf_repo/partial/signable-1.root.json`
* **DO** Send the signable partial root metadata file (`signable-1.root.json`) to each of the Key-holders.
    * **N.B.** This file only contains public keys. As such, no special precautions are required when sending the file.

