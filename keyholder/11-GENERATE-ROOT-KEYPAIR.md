# Ceremony: Provision Rugged Root keys

## title: TUF key generation and signing ceremonies runbook (aka "TUF Runbook")

This checklist describes the steps to perform an initial provisioning of Root
keys for a Rugged deployment.

## Steps

This runbook describes TUF key generation and signing ceremonies. The
procedures documented here are designed to implement the security policies for [INSERT YOUR PACKAGE REPOSITORY HERE].

If you need inspiration for security policies, please see [PEP 458: _Secure PyPI downloads with signed repository metadata_](https://www.python.org/dev/peps/pep-0458/)

All participants should be familiar with [Runbook notation](../README.md#notation).

## Start

* **GO TO** [Prepare the environment](#prepare-the-environment).

## Prepare the environment

* **DO** perform the [pre-ceremony](PRE-CEREMONY.md).
* **DO** Start streaming the ceremony using the communication computer.
* **IF** you have a phone or other personal devices, **THEN** set them on airplane mode.
* **DO** boot the trusted offline machine (the Raspberry Pi "ceremony computer"), and log into it using the credentials provided
during the pre-ceremony.
* **DO** mount the flash storage stick:

    ```bash
    $ sudo mount -t vfat /dev/sda1 /media/ceremony-products -o umask=000
    ```

* **DO** change directory to the runbook directory:

    ```bash
    $ cd ~/rugged-runbook-template
    ```

## Provision the Hardware Security Modules

* **DO** take pictures of each HSM, in their tamper-evident bags.

* **DO** remove `YubiHSM2-1` from its tamper-evident bag and **GO TO**
[Provisioning the YubiHSM 2](#provisioning-the-yubihsm-2)

* **DO** remove `YubiHSM2-2` from its tamper-evident bag and **GO TO**
[Provisioning the YubiHSM 2](#provisioning-the-yubihsm-2)

* **DO** remove `YubiHSM2-3` from its tamper-evident bag and **GO TO**
[Provisioning the YubiHSM 2](#provisioning-the-yubihsm-2)

* **DO** perform the [post-ceremony steps](#post-ceremony).

* **END**

You are now ready to [initialize the TUF repository](../init_tuf_repo).

## Provisioning the YubiHSM 2

*Time estimate: 10 minutes*.

* **DO** locate and write down the serial number printed on the YubiHSM 2. Refer to the picture below:

    ![A YubiHSM 2](./assets/yubihsm2.jpg)

    In this picture, the serial number is `7550054`. Note that in later steps the serial number will be 0-padded to 10 digits, like `0007550054`.

* **IF** the YubiHSM 2 is being reprovisioned due to a compromise or failed ceremony, **THEN** you must perform a physical reset.

    * **DO** touch and hold the metal contact of the YubiHSM 2 for ten (10) seconds as you insert it into the trusted offline computer.

* **IF** the YubiHSM 2 is being provisioned for the first time, **THEN** insert it into the trusted offline computer.

* **DO** ensure that exactly 1 (one) YubiHSM 2 is inserted into the trusted offline computer.

* **DO** Verify the serial number of the YubiHSM2 by running the following command:
    ```bash
    $ yubihsm-shell \
        --action=get-device-info | grep "Serial number:"
    ```
    Ref: [DEVICE INFO Command](https://docs.yubico.com/hardware/yubihsm-2/hsm-2-user-guide/hsm2-cmd-reference.html#device-info-command)

* **DO** Generate a new authentication key on the YubiHSM2, using the factory default authentication key (`1`) and password (`password`), by running the following command:
    ```bash
    $ yubihsm-shell \
        --action=put-authentication-key \
        --object-id=2 \
        --label=new_authentication_key \
        --domains=all \
        --capabilities=all \
        --delegated=all \
        --algorithm=ecp256 \
        --password=password \
        --new-password=<NEW PASSWORD>
    ```
    Ref: [PUT AUTHENTICATION KEY Command](https://docs.yubico.com/hardware/yubihsm-2/hsm-2-user-guide/hsm2-cmd-reference.html#put-authentication-key-command)

* **DO** Delete the factory-default authentication key on the YubiHSM2 by running the following command:
    ```bash
    $ yubihsm-shell \
        --action=delete-object \
        --object-id=1 \
        --object-type=authentication-key \
        --authkey=2
    ```
    Ref: [DELETE OBJECT Command](https://docs.yubico.com/hardware/yubihsm-2/hsm-2-user-guide/hsm2-cmd-reference.html#delete-object-command)

* **DO** When prompted, enter the authentication key password.

* **DO** Ensure that the factory-default authentication key has been deleted:
    ```
    $ yubihsm-shell --authkey=2 -a list-objects -i 1
    Found 0 object(s)
    ```

* **DO** Generate a new asymmetric keypair on the YubiHSM2 by running the following command:
    ```bash
    $ yubihsm-shell \
        --action=generate-asymmetric-key \
        --object-id=100 \
        --label=label_ecdsa_sign \
        --domains=1,2,3 \
        --capabilities=exportable-under-wrap,sign-eddsa \
        --algorithm=ed25519 \
        --authkey=2
    ```
    Ref: [GENERATE ASYMMETRIC KEY Command](https://docs.yubico.com/hardware/yubihsm-2/hsm-2-user-guide/hsm2-cmd-reference.html#generate-asymmetric-key-command)

* **DO** When prompted, enter the authentication key password.

* **DO** Export the public key from the YubiHSM2 by running the following command:
    ```bash
    $ yubihsm-shell \
        --action=get-public-key \
        --object-id=100 \
        --out=ceremony-products/XXXXXXXXXX/XXXXXXXXXX_root_pubkey.pub.pem \
        --outformat=PEM \
        --authkey=2
    ```
    Where `XXXXXXXXXX` is the 0-prefixed serial number.

* **DO** When prompted, enter the authentication key password.

* **DO** check for the following files in the runbook directory:
** TODO: UPDATE THIS TO REFLECT THE ARTIFACTS FROM `yubihsm-shell`**
    ```
    ceremony-products/XXXXXXXXXX/XXXXXXXXXX_root_pubkey.pub
    ```

    Where `XXXXXXXXXX` is the 0-prefixed serial number.

* **DO** remove the HSM.

* **DO** label a tamper-evident bag with the HSM's signing body ID and 0-prefixed serial number.

* **DO** seal the provisioned HSM and folded authentication key password in the tamper-evident bag.

* **DO** hold the sealed tamper-evident bag up to the camera of the communication computer.

## Post-ceremony

* **DO** insert the flash stick into the communication computer.

* **DO** navigate to the runbook repository in a new terminal.

* **DO** create a new branch:

    ```bash
    git checkout -b ceremony-YYYY-MM-DD
    ```

    Where `YYYY-MM-DD` is the current date.

* **DO** create the following new subdirectories:

    ```bash
    mkdir -p ceremony/YYYY-MM-DD/ceremony-products
    mkdir -p ceremony/YYYY-MM-DD/images
    ```

    Where `YYYY-MM-DD` is the current date.

* **DO** copy the contents of the ceremony flash stick into the `ceremony-products` subdirectory.

* **DO** copy all images taken of the HSMs and tamper-evident bags into the `images` subdirectory.

* **DO** commit the results, signing with a publicly announced PGP key:

    ```bash
    git add ceremony/YYYY-MM-DD
    git commit -S
    ```

    Where `YYYY-MM-DD` is the current date.

* **DO** push the branch to your fork of the [rugged/rugged-runbook-template](https://gitlab.com/rugged/rugged-runbook-template) and open
a PR for review.

    ```bash
    git push origin ceremony-YYYY-MM-DD
    ```

    Where `YYYY-MM-DD` is the current date.

* **DO** await for PR approval, and confirm that the branch is merged into the `main` branch.

    * You **MAY** delete the original ceremony branch once merged.

* **DO** securely destroy the SD card used for the runbook image **OR** zero it:
** TODO: IS THIS REALLY NECESSARY? IF YES, DOCUMENT _WHY?_ **
    ```bash
    $ diskutil unmountDisk /dev/rdiskN
    $ sudo dd bs=4m if=/dev/zero of=/dev/rdiskN
    ```
