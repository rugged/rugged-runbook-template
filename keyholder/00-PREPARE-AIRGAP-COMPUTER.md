# Prepare Air-gapped computer for performing ceremonies

Role: Keyholder

Any Rugged security ceremony will require an air-gapped computer prepared
carefully in order to produce trusted outputs. The steps below should be tested
and tuned as needed to ensure you have a secure workstation on which to perform
any Rugged ceremonies.

## Steps

- Acquire the following equipment:
    - Raspberry Pi
        - For development and testing, we used a Raspberry Pi 4 Model B (Purchased as a preassembled kit ([PI4-2GB-EXT128EW-C8-BLK-A](https://www.canakit.com/raspberry-pi-4-extreme-aluminum-case-kit.html)).
    - MicroSD Card (included in the kit above).
- Download the [Raspberry Pi OS Image](https://www.raspberrypi.com/software/operating-systems/)
    - Raspberry Pi OS Lite (64-bit, no desktop) -- Release date: December 11th 2023. Based on Debian 12 (Bookworm). Ref: [Image link](https://downloads.raspberrypi.com/raspios_lite_arm64/images/raspios_lite_arm64-2023-12-11/2023-12-11-raspios-bookworm-arm64-lite.img.xz)
    - Validate the image (hash: `9ce5e2c8c6c7637cd2227fdaaf0e34633e6ebedf05f1c88e00f833cbb644db4b`)
- Prepare the SD Card with the OS image:
    - Download and install the Raspberry Pi Imager: https://www.raspberrypi.com/software/
    - Write the image to the SD Card
- Start and login to the Raspberry Pi
- Install and verify the YubiHSM Shell:
    - Download the [YubiHSM SDK package (version `2023-11`)](https://developers.yubico.com/YubiHSM2/Releases/yubihsm2-sdk-2023-11-debian12-amd64.tar.gz) that will match the OS version (Debian 12) from [the Releases page](https://developers.yubico.com/YubiHSM2/Releases/)
    - Download the [signature](https://developers.yubico.com/YubiHSM2/Releases/yubihsm2-sdk-2023-11-debian12-amd64.tar.gz.sig)
    - [Verify the package](https://developers.yubico.com/Software_Projects/Software_Signing.html#_verifying_signatures_with_gnupg)
    - Install the package:
      ```bash
      $ tar -xvf yubihsm2-sdk-2023-11-debian12-amd64.tar.gz
      $ sudo dpkg -i \
          yubihsm2-sdk/libykhsmauth1_*.deb \
          yubihsm2-sdk/libyubihsm1_*.deb \
          yubihsm2-sdk/libyubihsm-http1_*.deb \
          yubihsm2-sdk/libyubihsm-usb1_*.deb \
          yubihsm2-sdk/yubihsm-auth_*.deb \
          yubihsm2-sdk/yubihsm-connector_*.deb \
          yubihsm2-sdk/yubihsm-pkcs11_*.deb \
          yubihsm2-sdk/yubihsm-setup_*.deb \
          yubihsm2-sdk/yubihsm-shell_*.deb \
          yubihsm2-sdk/yubihsm-wrap_*.deb
      ```
- (Optionally) Re-export the image for easier re-use by other keyholders.
- Shut down the Raspberry Pi
- Disconnect it from the network. This is now the "air-gapped computer" referenced in subsequent processes. From now on, this device should no longer be connected to any networks.

