# Update TUF repository with new Root keys

This ceremony describes the final step in Root key rotation: deploying the new
completed version of the Rugged TUF repository root metadata.

## Steps

* **DO** Pause processing of TUF metadata, and drain existing tasks:

    ```shell
    rugged pause-processing
    ```

* **DO** Copy `n+1.root.json` into place

    ```shell
    rugged deploy-updated-root-metadata (TODO: see [rugged#196](https://gitlab.com/rugged/rugged/-/issues/196))
    ```

For now:
    - copy root metadata into place (`cp $VERSION.root.json /path/to/rugged/repo`)
    - trigger re-signing process required (`rugged refresh-expiry`)

* **DO** Resume processing of TUF metadata:

    ```shell
    rugged resume-processing
    ```
