# Sign Partial Root Metadata

Role: Keyholder

## Sign partial root metadata with YubiHSM 2

* **DO** Insert the thumb drive into the preparation computer.

* **DO** Copy `signable-$VERSION.root.json` to the thumb drive. Eject the thumb drive.

* **DO** Insert the thumb drive into the ceremony computer.

* **DO** Generate a signature of the partial root metadata:
    ```
    $ yubihsm-shell \
        --action=sign-eddsa \
        --object-id=100 \
        --algorithm=ed25519 \
        --in=signable-$VERSION.root.json \
        --out=root_signature.bin \
        --outformat=binary \
        --auth-key=2
    ```

* **DO** Send the signature file (`root_signature.bin`) back to the Coordinator.

@TODO: fill in steps to commit && push ceremony branch.
  - copy the signature file back to thumb drive (or output it directly to thumb drive above)
  - remove thumb drive
  - place thumb drive in preparation computer
  - copy signature file into ceremony products branch
  - commit and push
