# Complete New Root Metadata

This document describes the steps performed by the Coordinator after they have
received signatures for an updated version of the Root metadata. The result is
a complete, signed next version of the Root metadata ready to be deployed to
the Rugged TUF repository.

## Steps

@TODO flesh this out, add prep/followup steps, etc.

* **DO** Add new root signatures for each Keyolder:

    ```shell
    rugged add-root-signature /path/to/verification_key.pem /path/to/signature
    ```

* **DO** Validate new root keys and signatures:
    ```shell
    rugged show-partial-root-metadata
    ```
