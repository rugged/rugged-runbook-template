# Provision Hardware Security Modules and SD cards?

Any ceremony will need to make use of a set of Hardware Security Modules on
which to generate Root keypairs to form the root of trust in a Rugged
deployment.

## Steps

The following is a checklist of action items that **MUST** be completed before the key signing and generation ceremonies may be attempted.

The entire pre-ceremony should take approximately *one hour* to complete.

## Inventory

Confirm the presence of **each** of the following:

* **Exactly one (1)** preparation computer
  * The preparation steps below assume that the computer is running macOS
  * This *MUST NOT* be the same as the ceremony computer, which is listed
    separately below
* **Exactly one (1)** communication computer
  * This *MAY* be the same as the preparation computer, but *MUST NOT* be the same as the ceremony computer
* **Exactly one (1)** digital camera
  * This *MAY* be a smartphone in airplane mode.
* **Exactly one (1)** external monitor, HDMI compatible
* **Exactly one (1)** HDMI cable
* **Exactly one (1)** USB keyboard
* **Exactly one (1)** microUSB to USB-A cable
* **Exactly one (1)** wall socket to USB-A power source, minimum 5V/1A
* **Exactly one (1)** Raspberry Pi as the ceremony computer
    * See [Prepare the air-gapped computer](../preparation/)
* **Exactly one (1)** microSD flash storage card, minimum 32GB
* **At least one (1)** microSD reader with USB interface
* **Exactly three (3)** HSMs:
  * **Exactly three (3)** YubiHSMs, with the following signing body IDs:
    * `YubiHSM2-1`
    * `YubiHSM2-2`
    * `YubiHSM2-3`
* **Exactly one (1)** USB flash storage stick, minimum 8GB
* **At least seven (4)** tamper-evident bags, including evidence labels
* **At least six (6)** sheets of loose-leaf paper
* **At least one (1)** permanent marker
* **At least one (1)** pair of scissors
* **At least one (1)** 6-sided die

Optionally, allow for **each** of the following:

* A USB-A extender

## Preparation

### Inspect the tamper-evident bags and markers

* Ensure that the tamper-evident bags are intact.
* Ensure that the markers are functional and can correctly mark the tamper-evident bags.

### Format the flash storage stick

On the preparation computer:

* Insert the flash storage stick.
* Open "Disk Utility" and identify the flash storage stick.
* Click "Unmount" if the flash storage stick is mounted.
* Click "Erase".
* Name the stick "TUF" and select `MS-DOS (FAT)` as the format.
* Click "Erase".
* Once formatting is complete, click "Unmount".
* Remove the flash storage stick from the preparation computer.

### Pre-generate HSM passwords and PINs

#### Pre-generate authentication key passwords for the YubiHSMs

For each of the YubiHSMs:

* Generate a 12-character authentication key password, using a 6-sided die and the following table:

    ```
                  First Roll
             1   2   3   4   5   6

    S  1     a   b   c   d   e   f
    e  2     g   h   i   j   k   l
    c  3     m   n   o   p   q   r
    o  4     s   t   u   v   w   x
    n  5     y   z   0   1   2   3
    d  6     4   5   6   7   8   9
    ```

    (Modified from [The Diceware FAQ](http://world.std.com/~reinhold/dicewarefaq.html))

* On a new sheet of paper, write down the authentication key password *along with the signing body ID*.
* Fold the paper.

#### Store the pre-generated passwords and PINs

* Once all YubiHSM authentication key passwords have been generated and written down, place them, **folded**, into a tamper-evident bag.
* Seal the bag.

### Test the ceremony communication computer

* Ensure that the communication computer's camera is functional.

* Ensure that the communication computer has internet access.

* Ensure that the communication computer's browser is up-to-date.

