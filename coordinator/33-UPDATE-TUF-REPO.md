# Update TUF repository with new Online keys

This document describes the final step in Online key rotation: deploying the
new completed version of the Rugged TUF repository root metadata.

**NB** These steps are largely the same as the
[coordinator/22-UPDATE-TUF-REPO.md](../coordinator/22-UPDATE-TUF-REPO.md) but
includes some extra steps due to the fact the Repository is already live and
processing metadata.

- in this Ceremony, we need to re-sign things:
  - pause-processing,
  - deploy new keys into appropriate workers, then
  - trigger re-signing/refresh, and
  - resume-processing
- during the initial deployment this isn't required, as the Repository is not yet live.
