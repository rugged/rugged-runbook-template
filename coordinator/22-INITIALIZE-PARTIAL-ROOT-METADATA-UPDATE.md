# Generate signable root metadata for update

This ceremony describes the initialization of an updated version of the root
metadata for an existing Rugged TUF repository deployment where root key
rotation is required.

This process assumes the keyholder(s) have already generated a new Root
keypair, and provided the public half to the coordinator. A next version of
Root metadata is created in "partial" form, containing the signable portion,
including new Root key(s), which can be distributed to keyholders for signing.

High-level:
 - generate new signable root metadata version (`n+1.root.json`) (see [rugged#163](https://gitlab.com/rugged/rugged/-/issues/163)
 - replace one or more Root keys with newly generated ones (see [rugged#164](https://gitlab.com/rugged/rugged/-/issues/164))

# Rotate Root keys

## Steps

* **DO** Navigate to the correct working directory on ceremony computer with ceremony branch checked out.

* **DO** Initialize new partial root metadata:

    ```shell
    rugged initialize-partial-root-metadata-for-update
    ```

* **DO** Identify KEYID of key(s) to be removed:

    ```shell
    rugged show-partial-root-metadata # TODO: Is this output sufficiently transparent to identify the correct KEYID?
    ```

* **DO** Remove deprecated or compromised root key(s):

    ```shell
    rugged remove-verification-key root KEYID
    ```

* **DO** Add new root key(s):

    ```shell
    rugged add-verification-key root /path/to/new-key-file.pem
    ```

* **DO** Send the resulting `n+1.root.json-signable` to keyholders for signing.
  - this can be a git commit and push the ceremony branch, for keyholders to pull

## Notes:

    - once all new keys are rotated in, we should probably verify the unsigned metadata
    - at the end here, commit signable portion to the ceremony branch, as its now fixed
    - a git push/pull can be the mechanism to distribute this to keyholders for signing
