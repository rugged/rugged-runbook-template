# Rugged Runbook Template

## What is this?

This repository is a baseline template for the core ceremonies required to
operate Rugged securely. It details the hardware requirements, preparatory
steps, and Runbooks needed for Provisioning and Rotating Root keys for a Rugged
deployment.

## Usage

The intended use is to fork this repository and customize the ceremonies as
needed, to document any specific steps or requirements for a particular
deployment, as well as to house the "ceremony products" resulting from such
ceremonies (eg. pictures of the HSM modules used, public keys generated, etc.)

It is inspired by the Python Software Foundation's TUF key generation and
signing ceremonies [runbook](https://github.com/psf/psf-tuf-runbook) and is
based on requirements for [securely managing offline keys](https://peps.python.org/pep-0458/#managing-offline-keys) as outlined in PEP-458.

Please report security issues to security@rugged.works. See https://rugged.works/SECURITY.md for details.

## Notation

These documents are designed to be read as a *runbook* -- a collection of discrete instructions with remediation steps that, if followed correctly, should result in the intended effects.

We use the following notation:

* **DO** *actions*: Perform the following actions.
* **IF** *condition* **THEN** *actions*: If *condition* is met, then perform the following *actions*.
* **GO TO** *heading*: Go to the referenced heading in the runbook and perform the stated actions thereon.
* **END**: You've reached an end state.


## Roles

In a typical Rugged deployment, there are 2 key roles required to perform these
ceremonies:

1. Coordinator - an administrative person who facilitates the overall process, collecting keys and signatures, and deploying TUF repository updates.
2. Keyholder - 1 or more people who will hold one of the set of Root keys for the Rugged deployment, generating Root keys on a Hardware Security Module (HSM) and signing Root metadata as required.

This template repository is organized around these roles, so each can see a
complete set of instructions and checklists for performing their part in the
ceremony.

The Ceremonies described below encompass the lifecycle of a Rugged TUF repository:

1. Initial deployment, where we generate Root keypairs, initialize Root metadata, then sign and deploy it.
2. Root (offline) key rotation, where we generate 1 or more new Root keypairs, then update and deploy the Root metadata with new signatures.
3. Online key rotation, where we generate 1 or more new online keypairs, then update and deploy the Root metadata with new signatures.

In each stage of the ceremonies, the coordinator typically initiates the
process, then instructs each keyholder to perform the corresponding steps of
their role. In some cases the coordinator also takes some final steps after the
keyholders have finished in order to complete the stage.

Note: The ceremonies described below are designed to be performed
asynchronously and with the parties involved distributed physically. This does
not preclude performing the ceremonies in the same physical space at the same
time, of course.

## TUF Repository Lifecycle Ceremonies

### 0. Prepare Hardware

- [coordinator/00-PREPARE-OS-IMAGE.md](coordinator/00-PREPARE-OS-IMAGE.md): Coordinator prepares an OS image
- [keyholder/00-PREPARE-AIRGAP-COMPUTER.md](keyholder/00-PREPARE-AIRGAP-COMPUTER.md): Keyholders each prepare airgapped computer

### 1. Initial Deployment Ceremony

#### 1.0 Prepare and initiate ceremony

- [coordinator/10-INITIATE-CEREMONY-BRANCH.md](coordinator/10-INITIATE-CEREMONY-BRANCH.md): Coordinator initiates ceremony branch for building new N.root.json metadata
- [keyholder/10-VERIFY-CEREMONY-COMPUTER.md](keyholder/10-VERIFY-CEREMONY-COMPUTER.md): Keyholders each validate the ceremony computer has trusted binaries.

#### 1.1 Generate Root keys

- [keyholder/11-PREPARE-ROOT-KEYPAIR-GENERATION.md](keyholder/11-PREPARE-ROOT-KEYPAIR-GENERATION.md): Keyholders each provision HSM, generate passwords, and test communication computer
- (on camera) [keyholder/11-GENERATE-ROOT-KEYPAIR.md](keyholder/11-GENERATE-ROOT-KEYPAIR.md): Keyholders each generate a keypair on the HSM.

#### 1.2. Prepare and sign Root metadata

- [coordinator/12-INITIALIZE-PARTIAL-ROOT-METADATA.md](coordinator/12-INITIALIZE-PARTIAL-ROOT-METADATA.md): Coordinator generates signable root metadata.
- [keyholder/12-SIGN-ROOT-METADATA.md](keyholder/12-SIGN-ROOT-METADATA.md): Keyholders each sign root metadata
- [coordinator/12-COMPLETE-SIGNED-ROOT-METADATA.md](coordinator/12-COMPLETE-SIGNED-ROOT-METADATA.md): Coordinator collects keyholder signatures to complete root metadata.

#### 1.3. Initialize TUF repository

- [coordinator/13-INIT-TUF-REPO.md](coordinator/13-INIT-TUF-REPO.md): Coordinator initializes and deploys the TUF repository using signed root metadata.
  - For first run: copy `1.root.json` into `metadata/` folder of new TUF repo and run `rugged initialize`

### 2. Root Key Rotation

#### 2.0. Prepare and initiate ceremony

- Are these different than the 1.0 versions above?
- [coordinator/20-INITIATE-CEREMONY-BRANCH.md](coordinator/20-INITIATE-CEREMONY-BRANCH.md)
- [keyholder/20-VERIFY-CEREMONY-COMPUTER.md](keyholder/20-VERIFY-CEREMONY-COMPUTER.md)

#### 2.1. Generate new Root key(s)

This stage is equivalent to the initial Key generation ceremony (1.1), except
there is either a new keyholder or a subset of existing keyholders generating a
new keypair.

**NB** Ensure you don't try to rotate too many root keys at once. You must
leave a $THRESHOLD of existing root keys intact for the TUF repository to
remain valid.

- Is this different from 1.1 above?
- (on camera) [keyholder/21-GENERATE-ROOT-KEYPAIR.md](keyholder/21-GENERATE-ROOT-KEYPAIR.md): One or more keyholders that need to rotate their keypair.

#### 2.2. Update and sign new Root metadata

- [coordinator/22-INITIALIZE-PARTIAL-ROOT-METADATA-UPDATE.md](coordinator/22-INITIALIZE-PARTIAL-ROOT-METADATA-UPDATE.md): Coordinator generates next version of root metadata, and rotates new root keys into place.
- [keyholder/22-SIGN-NEW-ROOT-METADATA.md](keyholder/22-SIGN-NEW-ROOT-METADATA.md): Keyholders sign new version of root metadata.
- [coordinator/22-COMPLETE-NEW-ROOT-METADATA.md](coordinator/22-COMPLETE-NEW-ROOT-METADATA.md): Coordinator produces complete, signed root metadata updated to next version.

#### 2.3 Update TUF repository with new Root metadata

- [coordinator/23-UPDATE-TUF-REPO.md](coordinator/23-UPDATE-TUF-REPO.md): Coordinator deploys the TUF repository with new Root metadata. (see [rugged#174](https://gitlab.com/rugged/rugged/-/issues/174))

### 3. Rotate Online Keys

- @todo: see also https://gitlab.com/rugged/rugged/-/issues/184 (online key rotation)

#### 3.0 Prepare and initiate ceremony

- Are these different than the 1.0 versions above?
- [coordinator/30-INITIATE-CEREMONY-BRANCH.md](coordinator/30-INITIATE-CEREMONY-BRANCH.md)
- [keyholder/30-VERIFY-CEREMONY-COMPUTER.md](keyholder/30-VERIFY-CEREMONY-COMPUTER.md)

#### 3.1 Generate new Online key(s)

This stage doesn't require Keyholder participation, the Coordinator can produce new keys.

- [coordinator/31-GENERATE-NEW-ONLINE-KEYS.md](coordinator/31-GENERATE-NEW-ONLINE-KEYS.md)

#### 3.2 Update and sign new Root metadata

- [coordinator/32-INITIALIZE-PARTIAL-ROOT-METDATA-UPDATE.md](coordinator/32-INITIALIZE-PARTIAL-ROOT-METADATA-UPDATE.md)
- [keyholder/32-SIGN-NEW-ROOT-METADATA.md](keyholder/32-SIGN-NEW-ROOT-METADATA.md)

#### 3.3 Update TUF repository with new Root metadata

- [coordinator/33-UPDATE-TUF-REPO.md](coordinator/33-UPDATE-TUF-REPO.md)

## Ceremony products

* [ceremony/](ceremony/README.md) folder structure for containing products of ceremonies.

## License

@TODO: For open source projects, say how it is licensed. AGPL is probably not appropriate here, this might almost be a CC license, as it's primarily shareable content?

## Project status

This repository is a work in progress as we develop the steps for a first deployment of Rugged, but we expect it to solidify into a usable reference over the coming weeks.
