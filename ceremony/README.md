# Rugged Ceremony artifacts

This folder contains the products of security ceremonies performed in support
of a Rugged deployment. These products serve as documentation and reference
material for the trust anchors of a deployment. This README describes the
structure of the `ceremony/` folder, and provides a template README for each
ceremony performed.

## Structure

For each ceremony performed, create a datestamp folder under `ceremony/` with the following sub-folders:

* `ceremony/2024-04-20/`
    * `ceremony-products/`
    * `images/`

## TEMPLATE

Copy this into a README.md file for each ceremony performed.

```
YYYY-MM-DD
==========

On YYYY-MM-DD, FIRSTNAME LASTNAME (@USERNAME) and FIRSTNAME LASTNAME (@USERNAME) conducted
the [key generation] ceremony. The ceremony was conducted with the runbook as of
commit [COMMIT HASH]()

Repository assets:

* [ceremony-products/](ceremony-products/): Public keys, certificates, and attestations produced during the ceremony.
* [images/](images/): Digital images of HSMs, tamper-evident packagaging, and other paraphenelia from the ceremony.
* [handwriting-disambiguation.png](handwriting-disambiguation.png): Recorded for disambiguation of handwriting for PINs included with each HSM.

External assets:

* [YouTube](): A live recording of the ceremony.
* [Internet Archive](): An archival copy of the above.

Third-party assets:

* [Notes](https://gist.github.com/sethmlarson/4d88566d662d3fa9697a6b3ea2cf0de9): Notes with timestamps taken during broadcast by FIRSTNAME LASTNAME (@USERNAME)
```
