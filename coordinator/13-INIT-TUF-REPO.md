# Initialize TUF repository with signed Root metadata

## Role: Coordinator
## Purpose:

This ceremony describes the initial creation of a Rugged TUF metadata using
signed Root metadata.

repository by using Root metadata that has been signed by the Root keyholders,
and using that as the basis to sign metadata for each of the subsequent Roles.

## Steps

* **DO** Change to Rugged deployment and validate `rugged` command is working:
    ```
    cd /var/rugged
    ddev rugged         # Should return help output from rugged.
    ```
* **DO** Initialize a TUF repo using the generated root metadata:
    ```
    $ chown 440 fixtures/tuf_repo/ -R
    $ cp fixtures/tuf_repo/partial/1.root.json fixtures/tuf_repo/metadata/
    $ rugged initialize --local
    Initializing new TUF repository at /var/rugged/tuf_repo.
    warning: No keys found for 'root'.
    warning: Initialized 'root' metadata from disk.
    If you did not intend to initialize with existing 'root' metadata then delete '1.root.json' and re-run this command.
    Updated targets metadata.
    Updated snapshot metadata.
    Updated timestamp metadata.
    TUF repository initialized.
    ```
* **DO** Validate the resulting TUF repo metadata:
    ```
    $ rugged validate metadata
    Metadata for the 'root' role is valid.
    Metadata for the 'timestamp' role is valid.
    Metadata for the 'snapshot' role is valid.
    Metadata for the 'targets' role is valid.
    ```
