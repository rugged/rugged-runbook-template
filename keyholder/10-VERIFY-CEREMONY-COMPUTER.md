# Verify Ceremony Computer

After the air-gapped ceremony computer is prepared, it is important to verify
the software binaries we will use in the ceremony, to ensure they match the
trusted versions documented here.

## Verify yubihsm-shell hash

### Test the Raspberry Pi and peripherals

* Connect the Raspberry Pi to all peripherals **except** power and the flash stick.

* Connect the Raspberry Pi to power, and confirm boot on the monitor.

* Log into the Raspberry Pi on the prompt with the following credentials:

    Username: `pi`

    Password: `raspberry`

* Insert the flash storage stick into the Raspberry Pi.

* Identify the flash storage stick's device and confirm that it mounts:

    ```bash
    $ sudo mount -t vfat /dev/sda1 /media/ceremony-products -o umask=000
    $ sudo umount /media/ceremony-products
    ```

* Confirm the presence of the following programs, using `which`:

** TODO: Update this with YubuHSM shell binaries **

    ```bash
    $ which pkcs11-tool
    /usr/bin/pkcs11-tool
    $ which yubihsm-provision
    /home/pi/psf-tuf-runbook/bin/yubihsm-provision
    $ which nitrohsm-provision
    /home/pi/psf-tuf-runbook/bin/nitrohsm-provision
    ```

1. Confirm the hash of the `yubihsm-provision` binary against the following checksum:

    * SHA2-256: `27db7eb5c86fec7a5df40fab84cb2e67961524c4a5eec6e3bdc5dac6e62904e9`

    ```bash
    $ shasum -a 256 $(which yubihsm-provision)
    ```

1. Confirm the hash of the `nitrohsm-provision` binary against the following checksum:

    * SHA2-256: `9088da489aa6d1697593ea7a7968f546bee01ba4a555fbb39c643ee44ede6613`

    ```bash
    $ shasum -a 256 $(which nitrohsm-provision)
    ```

* Power the Raspberry Pi off and disconnect all peripherals **except** for the microSD card
and flash stick.

    ```bash
    $ sudo shutdown
    ```

* Store the Raspberry Pi and attached microSD card and flash stick in a tamper-evident bag.

