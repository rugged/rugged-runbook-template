# Prepare OS Image for Rugged TUF repository ceremonies

This document describes how to prepare an OS image to be burned onto an SD
card for all ceremony computers. The image will be based on a minimal stock
Raspberry Pi (Debian) installation, with the tools required to perform any of
the Rugged ceremonies effectively.

This includes:
- git
- YubiHSM-shell (specific version/build)
- ?

@TBD
