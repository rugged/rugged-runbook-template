# Initiate Ceremony branch for Rugged artifacts

This document describes the preparation of a new Ceremony branch to contain the
artifacts produced by the steps performed by Coordinator and Keyholder to
produce a new Root metadata file and deploy it into a Rugged TUF repository.

@TBD

- basically a `git branch` operation
- naming is important, should probably correspond to the version of root.json
  being produced
- coordinator should initiate a new `ceremony/YYYY-MM-DD/README.md` (based on a
  template?) to describe what is being done and why.
  - also create new `ceremony/YYYY-MM-DD/{ceremony-products,images}/` dirs
- branch is pushed to a remote that keyholders have access to, and
  coordinator then instructs them to perform the next steps
